package com.controller;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.bean.Items;
import com.bean.UserOrder;
import com.dao.OrderDao;
import com.service.OrderService;


@Controller
public class orderController {
	
	@Autowired
	OrderService orderService;
	
	List<Items>listOfItems;
	
	@SuppressWarnings("unchecked")
	@PostMapping(value="order")
	public String getorder(HttpServletRequest req,HttpSession hs) {
		int itemId=Integer.parseInt(req.getParameter("itemId"));
		int noOfPlates=Integer.parseInt(req.getParameter("noOfPlates"));
		String name=(String) hs.getAttribute("name");
		String orderResult;
		listOfItems=(List<Items>) hs.getAttribute("items");
		Iterator<Items>li=listOfItems.iterator();
		while(li.hasNext()) {
			Items itm=li.next();
					
			if(itemId==itm.getItemId()) {
				UserOrder order=new UserOrder();
				order.setItemId(itm.getItemId());
				order.setItemName(itm.getItemName());
				order.setItemType(itm.getItemType());
				order.setItemPrice(itm.getItemPrice()*noOfPlates);			
				order.setName(name);
				orderResult=orderService.storeOrder(order);
			}
	                
		}	            
					return "menu";
	}
	
	@GetMapping(value="getList")
	public String  getOrderList(HttpSession hs){
		String name=(String) hs.getAttribute("name");
		List<UserOrder> res= orderService.getAllItemsOrdered(name);
		hs.setAttribute("lo", res);
		return "listOfOrder";
	}
	@GetMapping(value="getTotal")
	public String getTotal(HttpSession hs) {
		String name=(String) hs.getAttribute("name");
		float totalprice=orderService.getTotalBill(name);
		hs.setAttribute("totalbill", totalprice);
		return "finalbill";
	}
	
	
}
